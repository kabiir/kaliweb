from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

import tempfile
import os
import shutil

from PyQt4.Qt import (QWidgetAction, pyqtSignal, QObject, QToolButton, QMenu)

from calibre.gui2.actions import InterfaceAction
from calibre_plugins.kaliweb.main import KaliwebDialog
from calibre_plugins.kaliweb.common_utils import (set_plugin_icon_resources,
                                                  get_icon)

__license__   = 'GPL v3'
__copyright__ = '2014, Kabiir Maar'
__docformat__ = 'restructuredtext en'

if False:

    get_icons = get_resources = None

#-----------------------------------------------------------------------------
#- logging -------------------------------------------------------------------
from calibre_plugins.kaliweb.my_logger import get_logger
logger = get_logger('kaliweb', disabled=True)


PLUGIN_ICONS = ['images/icon.png', 'images/icon_connected.png']
PORTABLE_RESOURCES = [
    'portable/kaliweb.html',
    'portable/kaliweb.png']


class UnitedStates(QObject):
    library_changed = pyqtSignal()

    def __init__(self):
        QObject.__init__(self)
        self.portable_directory = tempfile.mkdtemp()
        self.initial = True

    def library_changed_emit(self):
        self.library_changed.emit()


class KaliwebUI(InterfaceAction):

    name = "Kaliweb"
    action_spec = ("Bring web to Calibre plugin",
                   'images/icon.png',
                   'Bring web to Calibre plugin',
                   None)
    action_add_menu = True
    dont_remove_from = frozenset(['toolbar', 'toolbar-device'])

    def genesis(self):
        icon_resources = self.load_resources(PLUGIN_ICONS)
        set_plugin_icon_resources(self.name, icon_resources)

        self.qaction.setIcon(get_icon(PLUGIN_ICONS[0]))
        self.old_actions_unique_map = {}
        self.us = UnitedStates()

        res = self.load_resources(PORTABLE_RESOURCES)
        os.makedirs(os.path.join(self.us.portable_directory, 'portable'))
        for resource in res.keys():
            logger.debug("RESOURCE KEY: {}".format(resource))
            with open(os.path.join(self.us.portable_directory,
                                   resource), 'wb') as portable:
                portable.write(res[resource])

        self.popup_type = QToolButton.InstantPopup

        base_plugin_object = self.interface_action_base_plugin
        do_user_config = base_plugin_object.do_user_config

        self.d = KaliwebDialog(self.gui,
                               self.qaction.icon(),
                               do_user_config,
                               self.qaction, self.us)
        m = QMenu(self.gui)
        self.qaction.setMenu(m)
        a = QWidgetAction(m)
        a.setDefaultWidget(self.d)
        m.addAction(a)

    def library_changed(self, db):
        self.us.library_changed_emit()
        print("library_change: {}".format(db.library_id))

    def apply_settings(self):
        from calibre_plugins.kaliweb.config import prefs
        prefs

    def shutting_down(self):
        logger.info("SHUTTING_DOWN... {}".format(self.us.portable_directory))
        if self.d.disconnect_all():
            shutil.rmtree(os.path.join(self.us.portable_directory))
            logger.info("DISCONNECT_ALL SUCCEEDED!")
            return True
        else:
            logger.info("DISCONNECT_ALL FAILED!")
            return False
